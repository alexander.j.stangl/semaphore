// Copyright 2019 Alexander John Stangl <alexander.j.stangl@freedommail.ch>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package semaphore implements a simple concurrency data
// structure to handle resource allocation management.
package semaphore

import (
	"container/heap"
	"sync"
)

type Interface interface {
	// Acquire requests a token, blocking until one is available.
	Acquire()

	// AcquireN requests n tokens, blocking until all are available.
	AcquireN(n int)

	// Release releases a token.
	Release()

	// ReleaseN releases n tokens.
	ReleaseN(n int)

	// Query queries whether a token is available, without blocking.
	Query() bool

	// QueryN queries whether n tokens are available, without blocking.
	QueryN(n int) bool

	// Capacity returns the number of tokens managed by the semaphore.
	Capacity() int

	// Allocated returns the number of tokens allocated.
	Allocated() int

	// Available returns the number of non-allocated tokens.
	Available() int

	// Kill cleans up any resources used by a semaphore.
	Kill()
}

type request struct {
	update   int
	callback chan<- struct{}
}

type requests []request

func (rs requests) Len() int {
	return len(rs)
}

func (rs requests) Less(i, j int) bool {
	return rs[i].update < rs[j].update
}

func (rs requests) Swap(i, j int) {
	rs[i], rs[j] = rs[j], rs[i]
}

func (rs *requests) Push(x interface{}) {
	*rs = append(*rs, x.(request))
}

func (rs *requests) Pop() interface{} {
	n := len(*rs)
	x := (*rs)[n-1]
	*rs = (*rs)[0 : n-1]
	return x
}

type semaphore struct {
	keys, max int
	waitlist  *requests
	incoming  chan request
	kill      chan struct{}
	sync.RWMutex
}

func (s semaphore) sane(update int) bool {
	t := s.keys - update
	return t >= 0 && t <= s.max
}

func New(n int) Interface {
	if n < 1 {
		panic("semaphore.New(): non-positive size argument")
	}
	s := &semaphore{
		n,
		n,
		&requests{},
		make(chan request),
		make(chan struct{}),
		sync.RWMutex{},
	}
	go func() {
		for {
			select {
			case <-s.kill:
				return
			case r := <-s.incoming:
				heap.Push(s.waitlist, r)
				s.Lock()
			loop:
				for s.waitlist.Len() > 0 {
					r := (*s.waitlist)[0]
					switch {
					case s.sane(r.update):
						r = heap.Pop(s.waitlist).(request)
						s.keys -= r.update
						r.callback <- struct{}{}
					case r.update < 0:
						panic("semaphore: freed non-allocated keys")
					default:
						break loop
					}
				}
				s.Unlock()
			}
		}
	}()
	return s
}

func (s *semaphore) Acquire() {
	s.AcquireN(1)
}

func (s *semaphore) AcquireN(n int) {
	if n > s.max {
		panic("semaphore.AcquireN(): more keys requested than exist")
	}
	callback := make(chan struct{})
	s.incoming <- request{n, callback}
	<-callback
	close(callback)
}

func (s *semaphore) Release() {
	s.AcquireN(-1)
}

func (s *semaphore) ReleaseN(n int) {
	s.AcquireN(-n)
}

func (s *semaphore) Query() bool {
	s.RLock()
	defer s.RUnlock()
	return s.keys >= 1
}

func (s *semaphore) QueryN(n int) bool {
	s.RLock()
	defer s.RUnlock()
	return s.keys >= n
}

func (s *semaphore) Capacity() int {
	return s.max
}

func (s *semaphore) Allocated() int {
	s.RLock()
	defer s.RUnlock()
	return s.max - s.keys
}

func (s *semaphore) Available() int {
	s.RLock()
	defer s.RUnlock()
	return s.keys
}

func (s *semaphore) Kill() {
	s.kill <- struct{}{}
	close(s.kill)
	close(s.incoming)
}
